-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 10, 2018 lúc 05:32 PM
-- Phiên bản máy phục vụ: 10.1.28-MariaDB
-- Phiên bản PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanlynhanvien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address`
--

CREATE TABLE `address` (
  `addressId` int(11) NOT NULL,
  `employeesID` int(11) NOT NULL,
  `sonha` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tenduong` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tenphuong` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tenquan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tenthanhpho` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `address`
--

INSERT INTO `address` (`addressId`, `employeesID`, `sonha`, `tenduong`, `tenphuong`, `tenquan`, `tenthanhpho`) VALUES
(9, 20, '8', 'Trần Cung', 'Cổ Nhuế', 'Bắc Từ Liêm', 'Hà Nội'),
(10, 21, '35', 'Đình Trung', 'Xuân Nộn', 'Đông Anh', 'Hà Nội'),
(11, 23, '8', 'Tân Gia ', 'Tân Gia', 'Tân Bình', 'Hồ Chí Minh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `ID` int(11) NOT NULL,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `type account` enum('','facebook','google') COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`ID`, `username`, `password`, `email`, `type account`, `images`) VALUES
(5, 'Ánh Nguyệt', 'e10adc3949ba59abbe56e057f20f883e', 'anhnguyet1997@gmail.com ', '', 'anhnguyet.jpg'),
(6, 'Mi Xu', '4d22345895759375074ce2c7004689fb', 'doanminhhiep1998@gmail.com', '', 'kimsohyun.jpg'),
(8, 'Lưu Hạo Nhiên', 'e10adc3949ba59abbe56e057f20f883e', 'haonhien1997@gmail.com', '', 'luuhaonhien.jpg'),
(9, 'Park Shin Hye', 'e10adc3949ba59abbe56e057f20f883e', 'parkshinhye1994@gmail.com', '', 'Park Shin Hye.jpg'),
(11, 'Dương Dương', 'e10adc3949ba59abbe56e057f20f883e', 'duongduong@gmail.com', '', 'duongduong.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `department`
--

CREATE TABLE `department` (
  `departmentID` int(11) NOT NULL,
  `departmentName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `department`
--

INSERT INTO `department` (`departmentID`, `departmentName`, `description`) VALUES
(1, 'Hành chính', ''),
(2, 'Kinh doanh', ''),
(3, 'Marketing', 'Quảng bá sản phẩm'),
(4, 'Kế Toán', 'Tính toán'),
(5, 'Nội vụ', 'Chức năng nhiệm vụ: tham mưu giúp UBND thị xã  thực hiện chức năng quản lý nhà nước các lĩnh vực'),
(6, 'Tư pháp', 'Chức năng, nhiệm vụ: tham mưu giúp UBND thị xã   thực hiện chức năng quản lý nhà nước về: công tác xây dựng văn bản quy phạm pháp luật; kiểm tra, xử lý văn bản quy phạm pháp luật.'),
(7, 'Kinh tế', 'Chức năng, nhiệm vụ: tham mưu, giúp UBND thị xã   thực hiện chức năng quản lý nhà nước về: nông nghiệp; lâm nghiệp, diêm nghiệp; thủy lợi; thủy sản; phát triển nông thôn.');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employees`
--

CREATE TABLE `employees` (
  `employeesID` int(11) NOT NULL,
  `employeesName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `departmentID` int(11) NOT NULL,
  `images` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `datecreated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `employees`
--

INSERT INTO `employees` (`employeesID`, `employeesName`, `birthday`, `email`, `departmentID`, `images`, `datecreated`) VALUES
(20, 'Lưu Hạo Nhiên', '1997-02-09', 'haonhien1997@gmail.com', 1, 'luuhaonhien.jpg', '2018-08-06'),
(21, 'Ánh Nguyệt', '1997-06-23', 'anhnguyet1997@gmail.com ', 3, 'anhnguyet.jpg', '2018-08-06'),
(22, 'Bùi Anh Tuấn', '1993-11-15', 'buianhtuan1993@gmail.com', 3, 'buianhtuan.jpg', '2018-08-06'),
(23, 'Phạm Minh Hằng', '1992-06-16', 'minhhang@gmail.com', 2, 'Park Min Young.jpg', '2018-08-06');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`addressId`),
  ADD KEY `employeesID` (`employeesID`);

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`departmentID`);

--
-- Chỉ mục cho bảng `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employeesID`),
  ADD KEY `departmentID` (`departmentID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address`
--
ALTER TABLE `address`
  MODIFY `addressId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `department`
--
ALTER TABLE `department`
  MODIFY `departmentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `employees`
--
ALTER TABLE `employees`
  MODIFY `employeesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`employeesID`) REFERENCES `employees` (`employeesID`);

--
-- Các ràng buộc cho bảng `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`departmentID`) REFERENCES `department` (`departmentID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
