<?php 		
include_once ("application/Models/Model.php");

	/**
	 * summary
	 */
	class Controller
	{
	    /**
	     * summary
	     */
	    public function loginUser()
	    {
	    	
	    	$username = isset($_POST['username'])? $_POST['username']:'';
	    	$password = md5(isset($_POST['password'])? $_POST['password']:'');
	    	if ($password !='' && $username !='') {
	    		$model = new Model();
	    		$user = $model->login($username, $password);
	    		if ($user) {
	    			$_SESSION['username'] = $username;
	    			echo "<script>alert('Xin chào ".$username.", bạn đã đăng nhập thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
	    			// $this->employees();
	    		}else {
	    			echo "<script>alert('Sai tên đăng nhập hoặc mật khẩu, mời nhập lại!');</script>";
	    			include_once ("application/Views/login.php");
	    			
	    		} 	
	    	}else {
	    		include_once ("application/Views/login.php");
	    	}

	    }
	    // function set_logout(){
	    // 	session_delete('username');
	    // }
	    public function logout()
	    {
	    	// $this->set_logout();
 			// redirect(base_url('?controller=login&action=view'));
	    	session_destroy();
	    	include_once ("application/Views/login.php");

	    }
	    public function registerUser()
	    {

	    	$username ="";
	    	$password ="";
	    	$email = "";
	    	$images ="";
	    	$path="public/images/";
	    	$name = $_FILES['images']['name'];
	    	$tmp_name = $_FILES['images']['tmp_name'];
	    	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    		if(isset($_POST["username"])){ $username = $_POST['username'];}
	    		if(isset($_POST["password"])){ $password = md5($_POST['password']);}
	    		if(isset($_POST["email"])){ $email = $_POST['email'];}
	    		move_uploaded_file($tmp_name,$path.$name);
	    		$images = $name;
	    		$model = new Model();
	    		$result = $model->register($username,$password,$email,$images);
	    		if ($result===True) {
	    			echo 'Bạn đã đăng ký thành công';
	    			echo 'Mời bạn đăng nhập lại';
	    			include_once ("application/Views/login.php");
	    		}else {
	    			echo 'Đăng ký thất bại';
	    			include_once ("application/Views/register.php");
	    		}

	    	}
	    }
	    public function loginfacebook()
	    {

	    	include_once ("application/login-with-facebook/index.php");
	    	// if($_SERVER["REQUEST_METHOD"] == "POST"){
		    // 	if(isset($_POST["id"])){ $id = $_POST['id'];}
		    // 	if(isset($_POST["name"])){ $username = $_POST['name'];}
		    // 	if(isset($_POST["email"])){ $email = $_POST['email'];}
		    // 	$model = new Model();
		    // 	$result = $model->facebook($id,$username,$email);
		    // 	if ($result===True) {
		    // 		echo "<script>alert('Xin chào ".$username.", bạn đã đăng nhập thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
		    // 	}
	    	// }
	    	
	    }
	    public function logingoogle()
	    {
	    	include_once ("application/login-with-google/index.php");

	    }

	    public function employees()
	    {
	    	$model= new Model();
	    	$result = $model->listemployees();
	    	if(mysqli_num_rows($result) > 0){
	    		include_once ("application/Views/employees.php");
	    	}else{
	    		include_once ("application/Views/employees.php");
	    	}
	    }
	    public function addEmployees()
	    {
	    	$employeesId ="";
	    	$employeesName ="";
	    	$birthday ="";
	    	$email ="";
	    	$departmentID="";
	    	$datecreated ="";
	    	$path="public/upload/";
	    	$name = $_FILES['images']['name'];
	    	$tmp_name = $_FILES['images']['tmp_name'];
	    	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    		if(isset($_POST["employeesName"])){ $employeesName = $_POST['employeesName'];}
	    		if(isset($_POST["birthday"])){ $birthday = $_POST['birthday'];}
	    		if(isset($_POST["email"])){ $email = $_POST['email'];}
	    		if(isset($_POST["departmentID"])){ $departmentID = $_POST['departmentID'];}
	    		if(isset($_POST["datecreated"])){ $datecreated = $_POST['datecreated'];}
	    		move_uploaded_file($tmp_name,$path.$name);
	    		$images = $name;
	    		$model = new Model();
	    		$result = $model->themEmployees($employeesName,$birthday,$email,$departmentID,$images,$datecreated);
	    		if ($result===True) {
	    			echo "<script>alert('Thêm nhân viên thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}else {
	    			echo "<script>alert('Thêm nhân viên thất bại!');</script>";
	    			include_once ("application/Views/add_employees.php");
	    		}

	    	}	
	    }
	    public function addDepartment()
	    {
	    	$departmentID ="";
	    	$departmentName ="";
	    	$description = "";
	    	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    		if(isset($_POST["departmentID"])){ $departmentID = $_POST['departmentID'];}
	    		if(isset($_POST["departmentName"])){ $departmentName = $_POST['departmentName'];}
	    		if(isset($_POST["description"])){ $description = $_POST['description'];}
	    		$model = new Model();
	    		$result = $model->themDepartment($departmentID,$departmentName,$description);
	    		if ($result===True) {
	    			
	    			echo "<script>alert('Thêm phòng ban thành công!');</script>";
	    			include_once ("application/Views/add_employees.php");
	    		}else {
	    			echo "<script>alert('Thêm phòng ban thất bại!');</script>";
	    			include_once ("application/Views/add_department.php");
	    		}

	    	}
	    }
	    public function addAddress()
	    {
	    	$employeesID ="";
	    	$sonha ="";
	    	$tenduong = "";
	    	$tenphuong = "";
	    	$tenquan = "";	
	    	$tenthanhpho = "";
	    	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    		if(isset($_POST["employeesID"])){ $employeesID = $_POST['employeesID'];}
	    		if(isset($_POST["sonha"])){ $sonha = $_POST['sonha'];}
	    		if(isset($_POST["tenduong"])){ $tenduong = $_POST['tenduong'];}
	    		if(isset($_POST["tenphuong"])){ $tenphuong = $_POST['tenphuong'];}
	    		if(isset($_POST["tenquan"])){ $tenquan = $_POST['tenquan'];}
	    		if(isset($_POST["tenthanhpho"])){ $tenthanhpho = $_POST['tenthanhpho'];}
	    		$model = new Model();
	    		$result = $model->themAddress($employeesID,$sonha,$tenduong,$tenphuong,$tenquan,$tenthanhpho);
	    		if ($result===True) {
	    			echo "<script>alert('Thêm địa chỉ thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}else {
	    			echo "<script>alert('Thêm địa chỉ thất bại!');window.location.href='?controller=themdiachi&action=view';</script>";
	    		}

	    	}
	    }

	    public function department()
	    {
	    	$table = "department";
	    	$model= new Model();
	    	$result = $model->listdepartment($table);
	    	if(mysqli_num_rows($result) > 0){
	    		include_once ("application/Views/departments.php");
	    	}else{
	    		include_once ("application/Views/add_department.php");
	    		echo"không result";
	    	}
	    }
	    public function editEmployees()
	    {
	    	
	    	$employeesID = isset($_GET['id']) ? $_GET['id'] : '';
	    	if ($employeesID){
	    		$model= new Model();
	    		$result =$model-> detailemployees($employeesID);
	    		include_once ("application/Views/edit_employees.php");
	    	}
	    	
	    	if($_SERVER["REQUEST_METHOD"] == "POST"){
	    		if(isset($_POST["employeesName"])){ $result['employeesName'] = $_POST['employeesName'];}
	    		if(isset($_POST["birthday"])){ $result['birthday'] = $_POST['birthday'];}
	    		if(isset($_POST["email"])){ $result['email'] = $_POST['email'];}
	    		if(isset($_POST["departmentID"])){ $result['departmentID'] = $_POST['departmentID'];}
	    		if(isset($_POST["images"])){ $result['images'] = $_POST['images'];}
	    		if(isset($_POST["datecreated"])){ $result['datecreated'] = $_POST['datecreated'];}
	    		if(isset($_POST["employeesID"])){ $result['employeesID'] = $_POST['employeesID'];}
	    		$employ = $model->edit($result['employeesID'],$result['employeesName'],$result['birthday'],$result['email'],$result['departmentID'],$result['images'],$result['datecreated']);
	    		if ($employ===True) {
	    			echo "<script>alert('Sửa nhân viên thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}else {
	    			echo "<script>alert('Sửa nhân viên thất bại!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}
	    		
	    	}
	    	
	    	
	    }
	    
	    public function deleteEmployees()
	    {
	    	if (isset($_GET['id'])) {
	    		$employeesID= $_GET['id'];
	    		$model= new Model();
	    		$result = $model->xoa($employeesID);
	    		$resul = $model->xoaaddress($employeesID);
	    		if ($result>0 && $resul>0) {
	    			echo "<script>alert('Xóa sản phẩm thành công!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}else {
	    			echo "<script>alert('Xóa sản phẩm thất bại!');window.location.href='?controller=nhanvien&action=view';</script>";
	    		}
	    	}
	    	
	    }
	    public function detail()
	    {


	    	if (isset($_GET['id'])) {
	    		$employeesID = isset($_GET['id']) ? $_GET['id'] : '';
	    		if ($employeesID){
	    			$model= new Model();
	    			$result =$model-> detailemployees($employeesID);
	    			$resul =$model-> detailaddress($employeesID);
	    			include_once ("application/Views/detailEmployees.php");
	    		}
	    	}

	    }
	    public function searchEmployees()
	    {
	    	if(isset($_POST['submit'])&& $_POST["search"] !='')
	    	{
	    		$search= $_POST['search'];
	    		$model = new Model();
	    		$result= $model->search($search);
	    		if(mysqli_num_rows($result) > 0){
	    			include_once ("application/Views/header.php");
	    			echo"Kết quả trả về với từ khóa <b>$search</b>";
	    			include_once ("application/Views/search.php");
	    		}else{
	    			echo 'Lỗi';
	    		}
	    		
	    	}
	    	
	    	
	    }
	}
	?>


