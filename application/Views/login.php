<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="public/css/style.css" type="text/css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>

  <h2>Đăng nhập tài khoản</h2>

  <form action="?controller=login&action=view" method="post">
    <div class="imgcontainer">
      <img src="public/images/login.jpg" alt="Avatar" class="avatar">
    </div>
    <div class="container">
      <input type="hidden" value="1" name="c">
      <label for="uname"><b>Username</b></label>
      
      <input type="text" placeholder="Enter Username" name="username" required>

      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
      
      <button type="submit">Login</button>
      <label>
        <input type="checkbox" checked="checked" name="remember"> Remember me
      </label>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <label>..or login with:</label><br> 
      
      <a href="?controller=facebook&action=view">
        <button type="button" class="facebtn"><i class="fab fa-facebook-f"></i> facebook</button>
      </a>
      <a href="?controller=google&action=view">
        <button type="button" class="googlebtn"><i class="fab fa-google-plus-g"></i> Google </button>
      </a>
      <span class="psw"> <a href="?controller=registerView&action=view">Đăng Ký</a></span>
    </div>
  </form>
</body>
<?php include_once ("footer.php") ?> 
</html>
<!-- https://www.facebook.com/dialog/oauth?client_id=499190610543953&redirect_uri=http://localhost:8080/project_quanlynhanvien/?controller=nhanvien&action=view&scope=public_profile -->