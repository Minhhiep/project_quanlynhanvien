<?php include_once ("header.php") ?>
<form action="?controller=themdiachi&action=view" method="post">
	<div class="container">

		<legend>Thêm địa chỉ</legend>
		<div class="form-group">
			<label for="">Mã nhân viên</label>
			<input type="text" name="employeesID" class="form-control" required>
			<label for="">Số nhà</label>
			<input type="text" name="sonha" class="form-control" required>
			<label for=""><br>Tên đường</label>
			<input type='text' name="tenduong" class="form-control" required>
			<label for=""><br>Phường/xã</label>
			<input type='text' name="tenphuong" class="form-control" required>
			<label for=""><br>Quận/huyện</label>
			<input type='text' name="tenquan" class="form-control" required>
			<label for=""><br>Thành phố</label>
			<input type='text' name="tenthanhpho" class="form-control" required>
			<button type="submit" class="btn btn-primary">Thêm</button>
		</div>
	</div>

</form>

<?php include_once ("footer.php") ?> 