<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="public/css/style1.css" type="text/css">

</head>
<body>

<form action="?controller=register&action=view" method="post" enctype="multipart/form-data">
  <div class="container">
    <h1>Đăng ký thành viên</h1>
    <p>Vui lòng điền vào những ô dưới đây để tạo một tài khoản.</p>
    <hr>

    
    <label for="user"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required>
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>
    <label for=""><b>Images</b></label>
    <input type="file" id="hinh" name="images">
    <hr>
    

    <button type="submit" class="registerbtn">Đăng Ký</button>
  </div>
  
  <div class="container signin">
    <p>Bạn đã có tài khoản? <a href="?controller=login&action=view">Đăng nhập</a>.</p>
  </div>
</form>

</body>
</html>