<?php 	
	include_once ("application/Controllers/Controller.php");
	define("IN_SITE", true);
	if (isset($_GET['controller'],$_GET['action'])) {
		$controller = $_GET['controller'];
		$action = $_GET['action'];
		switch ($controller) {
			case 'login':
				$controller = new Controller();
				$controller->loginUser();
				break;
			case 'register':
				$controller = new Controller();
				$controller->registerUser();
				break;
			case 'registerView':
				include_once ("application/Views/register.php");
				break;
			case 'logout':
					$controller = new Controller();
					$controller->logout();
					break;
			case 'google':
					$controller = new Controller();
					$controller->logingoogle();	
					break;
			case 'facebook':
				$controller = new Controller();
				$controller->loginfacebook();
				break;
			case 'nhanvien':
				$controller = new Controller();
				$controller->employees();
				break;
			case 'themView':
				include_once ("application/Views/add_employees.php");
				break;
			case 'them':
				$controller = new Controller();
				$controller->addEmployees();
				break;
			case 'viewphongban':
				include_once ("application/Views/add_department.php");
				break;
			case 'themphongban':
				$controller = new Controller();
				$controller->addDepartment();
				break;
			case 'diachi':
				include_once ("application/Views/add_address.php");
				break;
			case 'themdiachi':
				$controller = new Controller();
				$controller->addAddress();
				break;
			case 'dsphongban':
				$controller = new Controller();
				$controller-> department();
				break;
			case 'chitiet':
				$controller = new Controller();
				$controller-> detail();
				break;
			case 'sua':
				$controller = new Controller();
				$controller->editEmployees();
				break;
			case 'xoa':
				$controller = new Controller();
				$controller->deleteEmployees();
				break;
			case 'search':
				$controller = new Controller();
				$controller->searchEmployees();
				break;
			default:
				$controller = new Controller();
				$controller->employees();
				break;
		}
	}else {

		$controller = isset($_GET['controller']) ? $_GET['controller']:'Controller';
		$action = isset($_GET['action']) ? $_GET['action']: 'loginUser' ;
		$contler = new $controller();
		$contler->$action();	
		
	}

 ?>